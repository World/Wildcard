<h1 align="center">
  <img src="data/icons/hicolor/scalable/apps/com.felipekinoshita.Wildcard.svg" alt="Wildcard Icon" width="192" height="192"/>
  <br>
  Wildcard
</h1>

<p align="center"><strong>Test your regular expressions</strong></p>

<p align="center">
  <a href="https://flathub.org/apps/details/com.felipekinoshita.Wildcard">
    <img width="200" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.svg"/>
  </a>
  <br>
</p>

<p align="center">
  <a href="https://flathub.org/apps/details/com.felipekinoshita.Wildcard">
    <img alt="Flathub downloads" src="https://img.shields.io/badge/dynamic/json?color=informational&label=downloads&logo=flathub&logoColor=white&query=%24.installs_total&url=https%3A%2F%2Fflathub.org%2Fapi%2Fv2%2Fstats%2Fcom.felipekinoshita.Wildcard"/>
  </a>
</p>

<p align="center">
  <img src="/data/screenshots/preview.png" alt="Preview"/>
  <img src="/data/screenshots/dark.png" alt="Dark"/>
</p>

Wildcard gives you a nice and simple to use interface to test/practice regular expressions.

<!-- ## Translation -->

## Code of conduct

Wildcard follows the GNOME project [Code of Conduct](./code-of-conduct.md). All
communications in project spaces, such as the issue tracker are expected to follow it.

